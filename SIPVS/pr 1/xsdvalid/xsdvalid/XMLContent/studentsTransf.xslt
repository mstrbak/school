﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
>
    <xsl:output method="xml" indent="yes"/>

    <xsl:template match="/students">
        <table border="1">
          <tr>
            <th>Meno</th>
            <th>Priezvisko</th>
            <th>AIS ID</th>
            <th>Rok narodenia</th>
            <th>Pocet pokusov</th>
            <th>Status</th>
          </tr>
          <xsl:for-each select="student">
            <tr>
              <td>
                <xsl:value-of select="firstname"/>
              </td>
              <td>
                <xsl:value-of select="lastname"/>
              </td>
              <td>
                <xsl:value-of select="attribute::ais_id"/>
              </td>
              <td>
                <xsl:value-of select="birthdate"/>
              </td>
              <td>
                <xsl:value-of select="attempts"/>
              </td>
              <xsl:choose>
                <xsl:when test="@status = 'passed'">
                    <td bgcolor="green">
                    <xsl:value-of select="attribute::status"/>
                    </td>
                </xsl:when>
                <xsl:otherwise>
                  <td bgcolor="red">
                    <xsl:value-of select="attribute::status"/>
                  </td>
                </xsl:otherwise>
              </xsl:choose>
            </tr>
          </xsl:for-each>
        </table>
    </xsl:template>
</xsl:stylesheet>
