﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Linq;
using System.Xml.Xsl;
using xsdvalid.Models;
using Org.BouncyCastle.Asn1.X509;
using System.Security.Cryptography.X509Certificates;
using System.Globalization;
using System.Numerics;

namespace xsdvalid.Controllers
{
    public class HomeController : Controller
    {
        public static string ServerPath = HostingEnvironment.MapPath("~");
        public string[] errorCodes = new string[] { "OK",
                "Koreňový element musí obsahovať atribúty xmlns:xzep a xmlns:ds podľa profilu XADES_ZEP",
                "Kontrola obsahu ds:SignatureMethod a ds:CanonicalizationMethod – musia obsahovať URI niektorého z podporovaných algoritmov pre dané elementy podľa profilu XAdES_ZEP",
                "Kontrola obsahu ds:Transforms a ds:DigestMethod vo všetkých referenciách v ds:SignedInfo – musia obsahovať URI niektorého z podporovaných algoritmov podľa profilu XAdES_ZEP",
                "Overenie ostatných elementov profilu XAdES_ZEP--ds:Signature musí mať Id atribút a namespace xmlns:ds",
                "Overenie ostatných elementov profilu XAdES_ZEP--ds:SignatureValue musí mať Id atribút",
                "Overenie ostatných elementov profilu XAdES_ZEP--ds:KeyInfo musí mať Id atribút, a musí obsahovať ds:X509Data, ktorý obsahuje elementy: ds:X509Certificate, ds:X509IssuerSerial, ds:X509SubjectName a hodnoty elementov ds:X509IssuerSerial a ds:X509SubjectName musia súhlasiť s príslušnými hodnatami v certifikáte, ktorý sa nachádza v ds:X509Certificate,",
                "Overenie ostatných elementov profilu XAdES_ZEP--ds:SignatureProperties musí mať Id atribút a musi obsahovať dva elementy ds:SignatureProperty pre xzep:SignatureVersion a xzep:ProductInfos a obidva ds:SignatureProperty musia mať atribút Target nastavený na ds:Signature",
                "Overenie ostatných elementov profilu XAdES_ZEP--ds:Manifest musí mať Id atribút a namespace xmlns:ds",
                "Kontrola obsahu ds:Transforms a ds:DigestMethod vo všetkých referenciách v ds:Manifest – musia obsahovať URI niektorého z podporovaných algoritmov podľa profilu XAdES_ZEP",
                "ds:Manifest neobsahuje poporované URI v atribúte Type",
                "ds:Manifest neobsahuje práve jednu referenciu na ds:Object",
                "hodnoty elementov ds:X509IssuerSerial a ds:X509SubjectName musia súhlasiť s príslušnými hodnatami v certifikáte, ktorý sa nachádza v ds:X509Certificate ",
                "overenie existencie referencií v ds:SignedInfo a hodnôt atribútov Id a Type voči profilu XAdES_ZEP pre:ds:KeyInfo element,ds:SignatureProperties element,xades:SignedProperties element,všetky ostatné referencie v rámci ds:SignedInfo musia byť referenciami na ds:Manifest elementy",
                "timestamp"
            };
        public int isWrong = 0;

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Student student)
        {
            student.SaveToXml();

            return Redirect("~");
        }

        [HttpPost]
        public ActionResult ValidateXsd(Student student)
        {
            var xmlFolder = Path.Combine(ServerPath, @"XMLContent");
            var xmlFile = Path.Combine(xmlFolder, @"students.xml");
            var xsdFile = Path.Combine(xmlFolder, @"students.xsd");

            XmlReaderSettings settings = new XmlReaderSettings();
            settings.ValidationType = ValidationType.Schema;
            settings.Schemas.Add(null, xsdFile);

            XmlDocument xmlDoc = new XmlDocument();
            using (XmlReader r = XmlReader.Create(xmlFile, settings))
                try
                {
                    xmlDoc.Load(r);
                }
                catch (XmlSchemaValidationException)
                {
                    return Json(new { success = false });
                }


            return Json(new { success = true });
        }

        [HttpGet]
        public ActionResult TransformXml()
        {
            var serverPath = Server.MapPath("~");
            var xmlFolder = Path.Combine(serverPath, @"XMLContent");
            var xmlFile = Path.Combine(xmlFolder, @"students.xml");
            var xsltFile = Path.Combine(xmlFolder, @"studentsTransf.xslt");
            var outputFile = Path.Combine(xmlFolder, @"output.xml");
            
            XsltArgumentList args = new XsltArgumentList();
            XslCompiledTransform t = new XslCompiledTransform();
            t.Load(xsltFile);
            XmlReaderSettings settings = new XmlReaderSettings();
            //settings.DtdProcessing = DtdProcessing.Parse;
            //settings.ValidationType = ValidationType.DTD;
            HtmlString htmlString;
            using (XmlReader reader = XmlReader.Create(xmlFile, settings))
            {
                StringWriter writer = new StringWriter();
                try
                {
                    t.Transform(reader, args, writer);
                }
                catch (Exception)
                {
                    
                    throw;
                }
                htmlString = new HtmlString(writer.ToString());
            }

            //ViewBag.StudentXML = htmlString;
            //return View();
            return Content(htmlString.ToString(), "text/html");
        }

        [HttpGet]
        public ActionResult GetXmlString()
        {
            return Content(System.IO.File.ReadAllText(Server.MapPath("~/XMLContent/students.xml")), "text/xml");
        }

        [HttpGet]
        public ActionResult GetXslString()
        {
            return Content(System.IO.File.ReadAllText(Server.MapPath("~/XMLContent/studentsTransf.xslt")), "text/xml");
        }

        [HttpGet]
        public ActionResult GetXsdString()
        {
            return Content(System.IO.File.ReadAllText(Server.MapPath("~/XMLContent/students.xsd")), "text/xml");
        }

        [HttpPost]
        public ActionResult SaveSignedXml(string document)
        {
            var serverPath = Server.MapPath("~");
            var xmlFolder = Path.Combine(serverPath, @"XMLContent");
            var xmlFile = Path.Combine(xmlFolder, @"signedDocument.xml");

            if (!System.IO.File.Exists(xmlFile))
            {
                System.IO.File.Create(xmlFile).Close();
            }
            byte[] data = Convert.FromBase64String(document);
            string decodedSignedXml = Encoding.UTF8.GetString(data);
            System.IO.File.WriteAllText(xmlFile, decodedSignedXml);

            return Json(new { success = true });
        }


        [HttpPost]
        public ActionResult ExtendT()
        {
            var serverPath = Server.MapPath("~");
            var xmlFolder = Path.Combine(serverPath, @"XMLContent");
            var xmlFile = Path.Combine(xmlFolder, @"signedDocument.xml");
            var xmlFileDecoded = Path.Combine(xmlFolder, @"decodedSignedDocument.xml");
            TSDitec.TSSoap ts = new TSDitec.TSSoapClient();
            TSDitec.GetTimestampRequest request = new TSDitec.GetTimestampRequest();
            request.Body = new TSDitec.GetTimestampRequestBody();

            if (!System.IO.File.Exists(xmlFile))
            {
                return Json(new { success = false });
            }

            XmlDocument doc = new XmlDocument();
            doc.Load(xmlFile);

            XmlNodeList elemList = doc.GetElementsByTagName("ds:SignatureValue");
            //for (int i = 0; i < elemList.Count; i++)
            //{
            var bytes = Encoding.UTF8.GetBytes(elemList[0].InnerXml);
            request.Body.dataB64 = Convert.ToBase64String(bytes);

            var result64 = ts.GetTimestamp(request);

            byte[] resultData = Convert.FromBase64String(result64.Body.GetTimestampResult);
            //string decodedResult = Encoding.UTF8.GetString(resultData);

            Org.BouncyCastle.Tsp.TimeStampResponse decodedResponse = new Org.BouncyCastle.Tsp.TimeStampResponse(resultData);
            var signedData = decodedResponse.TimeStampToken.ToCmsSignedData();
            Org.BouncyCastle.Tsp.TimeStampToken tok = decodedResponse.TimeStampToken;

            Org.BouncyCastle.Tsp.TimeStampTokenInfo tstInfo = tok.TimeStampInfo;
            var genTime = tstInfo.GenTime.ToString("s");
            var failInfo = decodedResponse.GetFailInfo();
            Console.WriteLine(failInfo);

            //}

            XmlNodeList qualifyingPropList = doc.GetElementsByTagName("xades:QualifyingProperties");
            XmlNode qualifyingProp = qualifyingPropList[0];
            if (qualifyingProp == null)
                return Json(new { success = false });

            XmlElement unProp = doc.CreateElement("xades", "UnsignedProperties", "http://uri.etsi.org/01903/v1.3.2#");
            XmlElement unSigProp = doc.CreateElement("xades", "UnsignedSignatureProperties", "http://uri.etsi.org/01903/v1.3.2#");
            XmlElement sigTmSt = doc.CreateElement("xades", "SignatureTimeStamp", "http://uri.etsi.org/01903/v1.3.2#");
            sigTmSt.InnerText = genTime;

            unSigProp.AppendChild(sigTmSt);
            unProp.AppendChild(unSigProp);
            qualifyingProp.AppendChild(unProp);
            
            doc.Save(xmlFileDecoded);

            return Json(new { success = true });
        }

        [HttpPost]
        public ActionResult Upload()
        {
            var serverPath = Server.MapPath("~");
            var xmlFolder = Path.Combine(serverPath, @"XMLContent\Upload");

            if (Request.Files.Count > 0)
            {
                foreach (string fileNameKey in Request.Files)
                {
                    var file = Request.Files[fileNameKey];
                    if (file != null && file.ContentLength > 0)
                    {
                        var fileName = Path.GetFileName(file.FileName);
                        var path = Path.Combine(Server.MapPath("~/XMLContent/Upload"), fileName);
                        file.SaveAs(path);
                    }

                    var nameUploaded = Path.GetFileName(file.FileName);
                    var xmlFile = Path.Combine(xmlFolder, nameUploaded);
                    var logFile = Path.Combine(xmlFolder, @"logFile");

                    XmlDocument doc = new XmlDocument();
                    doc.Load(xmlFile);

                    
                    var result = checkEnvelope1(doc);
                    if (result != 0)
                    {
                        System.IO.File.AppendAllText(logFile, DateTime.Now + " " + nameUploaded + "----" + errorCodes[result] + Environment.NewLine);
                        return RedirectToAction("Index");
                    }
                    result = checkTimestamp14(doc);
                    if (result != 0)
                    {
                        System.IO.File.AppendAllText(logFile, DateTime.Now + " " + nameUploaded + "----" + errorCodes[result] + Environment.NewLine);
                        return RedirectToAction("Index");
                    }
                    result = checkSignature2(doc);
                    if (result != 0)
                    {
                        System.IO.File.AppendAllText(logFile, DateTime.Now + " " + nameUploaded + "----" + errorCodes[result] + Environment.NewLine);
                        return RedirectToAction("Index");
                    }
                    result = checkTransformDigest3(doc);
                    if (result != 0)
                    {
                        System.IO.File.AppendAllText(logFile, DateTime.Now + " " + nameUploaded + "----" + errorCodes[result] + Environment.NewLine);
                        return RedirectToAction("Index");
                    }
                    result = checkSignature4(doc);
                    if (result != 0)
                    {
                        System.IO.File.AppendAllText(logFile, DateTime.Now + " " + nameUploaded + "----" + errorCodes[result] + Environment.NewLine);
                        return RedirectToAction("Index");
                    }
                    result = checkSignatureValue5(doc);
                    if (result != 0)
                    {
                        System.IO.File.AppendAllText(logFile, DateTime.Now + " " + nameUploaded + "----" + errorCodes[result] + Environment.NewLine);
                        return RedirectToAction("Index");
                    }
                    result = checkKeyInfo6(doc);
                    if (result != 0)
                    {
                        System.IO.File.AppendAllText(logFile, DateTime.Now + " " + nameUploaded + "----" + errorCodes[result] + Environment.NewLine);
                        return RedirectToAction("Index");
                    }
                    result = checkSignatureProperties7(doc);
                    if (result != 0)
                    {
                        System.IO.File.AppendAllText(logFile, DateTime.Now + " " + nameUploaded + "----" + errorCodes[result] + Environment.NewLine);
                        return RedirectToAction("Index");
                    }
                    result = checkManifest8(doc);
                    if (result != 0)
                    {
                        System.IO.File.AppendAllText(logFile, DateTime.Now + " " + nameUploaded + "----" + errorCodes[result] + Environment.NewLine);
                        return RedirectToAction("Index");
                    }
                    result = checkX509Issuer12(doc);
                    if (result != 0)
                    {
                        System.IO.File.AppendAllText(logFile, DateTime.Now + " " + nameUploaded + "----" + errorCodes[result] + Environment.NewLine);
                        return RedirectToAction("Index");
                    }
                    result = checkSignedInfoRefs13(doc);
                    if (result != 0)
                    {
                        System.IO.File.AppendAllText(logFile, DateTime.Now + " " + nameUploaded + "----" + errorCodes[result] + Environment.NewLine);
                        return RedirectToAction("Index");
                    }
                    
                }
            }

            return RedirectToAction("Index");
        }


        private int checkEnvelope1(XmlDocument doc)
        {
            XmlNodeList envelopeList = doc.GetElementsByTagName("xzep:DataEnvelope");

            XmlNode envelope = envelopeList[0];
            if (envelope.Attributes["xmlns:ds"].Value.Equals("http://www.w3.org/2000/09/xmldsig#"))
            {
                isWrong = 0;
            }
            else
            {
                isWrong = 1;
                return isWrong;
            }

            if (envelope.Attributes["xmlns:xzep"].Value.Equals("http://www.ditec.sk/ep/signature_formats/xades_zep/v1.0"))
            {
                isWrong = 0;
            }
            else
            {
                isWrong = 1;
                return isWrong;
            }

            return isWrong;
        }

        private int checkSignature2(XmlDocument doc)
        {

            var signatureMethAlgs = new string[] { "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256",
                                                    "http://www.w3.org/2001/04/xmldsig-more#rsa-sha384",
                                                    "http://www.w3.org/2001/04/xmldsig-more#rsa-sha512",
                                                    "http://www.w3.org/2000/09/xmldsig#dsa-sha1",
                                                    "http://www.w3.org/2000/09/xmldsig#rsa-sha1"};

            var signatureCanonAlgs = new string[] { "http://www.w3.org/TR/2001/REC-xml-c14n-20010315",
                                                    "http://www.w3.org/2006/12/xml-c14n11"};

            XmlNode sigMethod = doc.GetElementsByTagName("ds:SignatureMethod")[0];
            if (signatureMethAlgs.Contains(sigMethod.Attributes["Algorithm"].Value))
            {
                isWrong = 0;
            }
            else
            {
                isWrong = 2;
                return isWrong;
            }

            XmlNode sigCanon = doc.GetElementsByTagName("ds:CanonicalizationMethod")[0];
            if (signatureCanonAlgs.Contains(sigCanon.Attributes["Algorithm"].Value))
            {
                isWrong = 0;
            }
            else
            {
                isWrong = 2;
                return isWrong;
            }

            return isWrong;

        }


        private int checkTransformDigest3(XmlDocument doc)
        {
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
            nsmgr.AddNamespace("ds", "http://www.w3.org/2000/09/xmldsig#");
            nsmgr.AddNamespace("xzep", "http://www.ditec.sk/ep/signature_formats/xades_zep/v1.0");
            nsmgr.AddNamespace("xades", "http://uri.etsi.org/01903/v1.3.2#");

            var transformAlgs = new string[] { "http://www.w3.org/TR/2001/REC-xml-c14n-20010315",
                                               "http://www.w3.org/2000/09/xmldsig#base64"};

            var digestAlgs = new string[] { "http://www.w3.org/2000/09/xmldsig#sha1",
                                            "http://www.w3.org/2001/04/xmldsigmore#sha224",
                                            "http://www.w3.org/2001/04/xmlenc#sha256",
                                            "http://www.w3.org/2001/04/xmldsigmore#sha384",
                                            "http://www.w3.org/2001/04/xmlenc#sha512"};

            var transMethods = doc.SelectNodes("//ds:SignedInfo/ds:Reference/ds:Transforms/ds:Transform", nsmgr);
            if (transMethods.Count == 0)
            {
                isWrong = 3;
                return isWrong;
            }
            foreach (XmlNode transMethod in transMethods)
            {
                if (transformAlgs.Contains(transMethod.Attributes["Algorithm"].Value))
                {
                    isWrong = 0;
                }
                else
                {
                    isWrong = 3;
                    return isWrong;
                }
            }

            var sigDigests = doc.SelectNodes("//ds:SignedInfo/ds:Reference/ds:DigestMethod", nsmgr);
            if (sigDigests.Count == 0)
            {
                isWrong = 3;
                return isWrong;
            }
            foreach (XmlNode sigDigest in sigDigests)
            {
                if (digestAlgs.Contains(sigDigest.Attributes["Algorithm"].Value))
                {
                    isWrong = 0;
                }
                else
                {
                    isWrong = 3;
                    return isWrong;
                }
            }

            return isWrong;
        }

        private int checkSignature4(XmlDocument doc)
        {
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
            nsmgr.AddNamespace("ds", "http://www.w3.org/2000/09/xmldsig#");
            nsmgr.AddNamespace("xzep", "http://www.ditec.sk/ep/signature_formats/xades_zep/v1.0");
            nsmgr.AddNamespace("xades", "http://uri.etsi.org/01903/v1.3.2#");

            var signatures = doc.SelectNodes("//ds:Signature", nsmgr);
            if (signatures.Count == 0)
            {
                isWrong = 4;
                return isWrong;
            }
            foreach (XmlNode signature in signatures)
            {
                if (signature.Attributes["Id"] != null)
                {
                    isWrong = 0;
                }
                else
                {
                    isWrong = 4;
                    return isWrong;
                }
            }
            
            foreach (XmlNode signature in signatures)
            {
                if (signature.Attributes["xmlns:ds"].Value.Equals("http://www.w3.org/2000/09/xmldsig#"))
                {
                    isWrong = 0;
                }
                else
                {
                    isWrong = 4;
                    return isWrong;
                }
            }

            return isWrong;
        }

        private int checkSignatureValue5(XmlDocument doc)
        {
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
            nsmgr.AddNamespace("ds", "http://www.w3.org/2000/09/xmldsig#");
            nsmgr.AddNamespace("xzep", "http://www.ditec.sk/ep/signature_formats/xades_zep/v1.0");
            nsmgr.AddNamespace("xades", "http://uri.etsi.org/01903/v1.3.2#");

            var signatures = doc.SelectNodes("//ds:SignatureValue", nsmgr);
            if (signatures.Count == 0)
            {
                isWrong = 5;
                return isWrong;
            }
            foreach (XmlNode signature in signatures)
            {
                if (signature.Attributes["Id"] != null)
                {
                    isWrong = 0;
                }
                else
                {
                    isWrong = 5;
                    return isWrong;
                }
            }

            return isWrong;
        }

        private int checkKeyInfo6(XmlDocument doc)
        {
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
            nsmgr.AddNamespace("ds", "http://www.w3.org/2000/09/xmldsig#");
            nsmgr.AddNamespace("xzep", "http://www.ditec.sk/ep/signature_formats/xades_zep/v1.0");
            nsmgr.AddNamespace("xades", "http://uri.etsi.org/01903/v1.3.2#");

            var keys = doc.SelectNodes("//ds:KeyInfo", nsmgr);
            if (keys.Count == 0)
            {
                isWrong = 6;
                return isWrong;
            }
            foreach (XmlNode key in keys)
            {
                if (key.Attributes["Id"] != null)
                {
                    isWrong = 0;
                }
                else
                {
                    isWrong = 6;
                    return isWrong;
                }
            }

            var list509Data = doc.SelectNodes("//ds:KeyInfo/ds:X509Data", nsmgr);
            if (list509Data.Count == 0)
            {
                isWrong = 6;
                return isWrong;
            }

            String[] elements509Data = new String[] 
            {
                "ds:X509Certificate",
                "ds:X509IssuerSerial",
                "ds:X509SubjectName"
            };

            foreach (XmlNode child in list509Data[0].ChildNodes)
            {
                if (elements509Data.Contains(child.Name))
                {
                    isWrong = 0;
                }
                else
                {
                    isWrong = 6;
                    return isWrong;
                }
            }

            return isWrong;
        }

        private int checkSignatureProperties7(XmlDocument doc)
        {
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
            nsmgr.AddNamespace("ds", "http://www.w3.org/2000/09/xmldsig#");
            nsmgr.AddNamespace("xzep", "http://www.ditec.sk/ep/signature_formats/xades_zep/v1.0");
            nsmgr.AddNamespace("xades", "http://uri.etsi.org/01903/v1.3.2#");

            var sigProps = doc.SelectNodes("//ds:SignatureProperties", nsmgr);
            if (sigProps.Count == 0)
            {
                isWrong = 7;
                return isWrong;
            }
            foreach (XmlNode sigProp in sigProps)
            {
                if (sigProp.Attributes["Id"] != null)
                {
                    isWrong = 0;
                }
                else
                {
                    isWrong = 7;
                    return isWrong;
                }
            }

            // get ds:Signature ID
            var signature = doc.SelectNodes("//ds:Signature", nsmgr);
            var signatureId = '#'+signature[0].Attributes["Id"].Value;
            var signatureChilds = new string[] { "xzep:SignatureVersion",
                                               "xzep:ProductInfos"};

            var signatureProperties = doc.SelectNodes("//ds:SignatureProperties/ds:SignatureProperty", nsmgr);
            if (signatureProperties.Count != 2)
            {
                isWrong = 7;
                return isWrong;
            }
            foreach (XmlNode property in signatureProperties)
            {
                if (!signatureChilds.Any(x => new List<XmlNode>(property.ChildNodes.Cast<XmlNode>()).Any(y => y.Name == x)))
                {
                    isWrong = 7;
                    return isWrong;
                }

                if (property.Attributes["Target"] != null)
                {
                    isWrong = 0;
                    if (!signatureId.Equals(property.Attributes["Target"].Value))
                    {
                        isWrong = 7;
                        return isWrong;
                    }
                }
                else
                {
                    isWrong = 5;
                    return isWrong;
                }
            }
            return isWrong;
        }

        private int checkManifest8(XmlDocument doc)
        {
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
            nsmgr.AddNamespace("ds", "http://www.w3.org/2000/09/xmldsig#");
            nsmgr.AddNamespace("xzep", "http://www.ditec.sk/ep/signature_formats/xades_zep/v1.0");
            nsmgr.AddNamespace("xades", "http://uri.etsi.org/01903/v1.3.2#");

            var manifests = doc.SelectNodes("//ds:Manifest", nsmgr);
            if (manifests.Count == 0)
            {
                isWrong = 8;
                return isWrong;
            }
            foreach (XmlNode manifest in manifests)
            {
                if (manifest.Attributes["Id"] != null)
                {
                    isWrong = 0;
                }
                else
                {
                    isWrong = 8;
                    return isWrong;
                }
            }
            

            var transformAlgs = new string[] { "http://www.w3.org/TR/2001/REC-xml-c14n-20010315",
                                               "http://www.w3.org/2000/09/xmldsig#base64"};

            var digestAlgs = new string[] { "http://www.w3.org/2000/09/xmldsig#sha1",
                                            "http://www.w3.org/2001/04/xmldsigmore#sha224",
                                            "http://www.w3.org/2001/04/xmlenc#sha256",
                                            "http://www.w3.org/2001/04/xmldsigmore#sha384",
                                            "http://www.w3.org/2001/04/xmlenc#sha512"};

            var transMethods = doc.SelectNodes("//ds:Manifest/ds:Reference/ds:Transforms/ds:Transform", nsmgr);
            if (transMethods.Count == 0)
            {
                isWrong = 9;
                return isWrong;
            }
            foreach (XmlNode transMethod in transMethods)
            {
                if (transformAlgs.Contains(transMethod.Attributes["Algorithm"].Value))
                {
                    isWrong = 0;
                }
                else
                {
                    isWrong = 9;
                    return isWrong;
                }
            }

            var sigDigests = doc.SelectNodes("//ds:Manifest/ds:Reference/ds:DigestMethod", nsmgr);
            if (sigDigests.Count == 0)
            {
                isWrong = 9;
                return isWrong;
            }
            foreach (XmlNode sigDigest in sigDigests)
            {
                if (digestAlgs.Contains(sigDigest.Attributes["Algorithm"].Value))
                {
                    isWrong = 0;
                }
                else
                {
                    isWrong = 9;
                    return isWrong;
                }
            }

            var types = new string[] { "http://www.w3.org/2000/09/xmldsig#Object",
                                            "http://www.w3.org/2000/09/xmldsig#SignatureProperties",
                                            "http://uri.etsi.org/01903#SignedProperties",
                                            "http://www.w3.org/2000/09/xmldsig#Manifest"};
            
            var manRefs = doc.SelectNodes("//ds:Manifest/ds:Reference", nsmgr);
            if (manRefs.Count == 0)
            {
                isWrong = 10;
                return isWrong;
            }
            foreach (XmlNode manRef in manRefs)
            {
                if (types.Contains(manRef.Attributes["Type"].Value))
                {
                    isWrong = 0;
                }
                else
                {
                    isWrong = 10;
                    return isWrong;
                }
            }

            manRefs = doc.SelectNodes("//ds:Manifest", nsmgr);
            var references = 0;

            foreach (XmlNode manRef in manRefs)
            {
                var childs = manRef.ChildNodes;
                references = 0;
                foreach (XmlNode child in childs)
                {
                    if ("ds:Reference".Equals(child.Name))
                    {
                        var attrs = child.Attributes;
                        var uri = attrs.GetNamedItem("URI").InnerText;
                        uri = uri.Substring(1);

                        var obj = doc.SelectNodes("//ds:Object[@Id='" + uri + "']", nsmgr);
                        if (obj.Count == 1) references++;
                    }
                }

                if (references != 1)
                {
                    isWrong = 11;
                    return isWrong;
                }
            }
            return isWrong;
        }

        private int checkX509Issuer12(XmlDocument doc)
        {
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
            nsmgr.AddNamespace("ds", "http://www.w3.org/2000/09/xmldsig#");
            nsmgr.AddNamespace("xzep", "http://www.ditec.sk/ep/signature_formats/xades_zep/v1.0");
            nsmgr.AddNamespace("xades", "http://uri.etsi.org/01903/v1.3.2#");

            var certNode = doc.SelectNodes("//ds:X509Certificate", nsmgr);
            if (certNode.Count == 0)
            {
                isWrong = 12;
                return isWrong;
            }

            byte[] data = Convert.FromBase64String(certNode[0].InnerText);
            X509Certificate cert = new X509Certificate(data);
            var decValue = BigInteger.Parse(cert.GetSerialNumberString(), NumberStyles.HexNumber);

            var dsSubjectName = cert.Subject;
            var issuerName = cert.Issuer;
            var serialNumber = decValue.ToString();


            var issuerNameNodeList = doc.SelectNodes("//ds:X509IssuerSerial/ds:X509IssuerName", nsmgr);
            var serialNumberNodeList = doc.SelectNodes("//ds:X509IssuerSerial/ds:X509SerialNumber", nsmgr);
            var subjectNameNodeList = doc.SelectNodes("//ds:X509SubjectName", nsmgr);

            if (!(issuerName.Equals(issuerNameNodeList[0].InnerText) && serialNumber.Equals(serialNumberNodeList[0].InnerText) && dsSubjectName.Equals(subjectNameNodeList[0].InnerText)))
            {
                isWrong = 12;
                return isWrong;
            }

            Debug.WriteLine(serialNumber);

            return isWrong;
        }


        private int checkSignedInfoRefs13(XmlDocument doc)
        {
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
            nsmgr.AddNamespace("ds", "http://www.w3.org/2000/09/xmldsig#");
            nsmgr.AddNamespace("xzep", "http://www.ditec.sk/ep/signature_formats/xades_zep/v1.0");
            nsmgr.AddNamespace("xades", "http://uri.etsi.org/01903/v1.3.2#");

            var keyInfoNode = doc.SelectNodes("//ds:KeyInfo", nsmgr);
            if (keyInfoNode.Count == 0)
            {
                isWrong = 13;
                return isWrong;
            }

            var sigPropsNode = doc.SelectNodes("//ds:SignatureProperties", nsmgr);
            if (sigPropsNode.Count == 0)
            {
                isWrong = 13;
                return isWrong;
            }

            var signedPropsNode = doc.SelectNodes("//xades:SignedProperties", nsmgr);
            if (signedPropsNode.Count == 0)
            {
                isWrong = 13;
                return isWrong;
            }

            var keyInfoAttr = keyInfoNode[0].Attributes["Id"].Value;
            var signaturePropsAttr = sigPropsNode[0].Attributes["Id"].Value;
            var signedPropsAttr = signedPropsNode[0].Attributes["Id"].Value;


            var signedInfoRefNodeList = doc.SelectNodes("//ds:SignedInfo/ds:Reference", nsmgr);

            int keyCounter = 0;
            int sigCounter = 0;
            int signedCounter = 0;
            foreach (XmlNode reference in signedInfoRefNodeList)
            {
                var idRef = "Reference" + signedPropsAttr;
                if (reference.Attributes["URI"].Value.Equals('#' + keyInfoAttr))
                {
                    if (!idRef.Equals(reference.Attributes["Id"].Value) || !reference.Attributes["Type"].Value.Equals("http://www.w3.org/2000/09/xmldsig#Object"))
                    {
                        isWrong = 13;
                        return isWrong;
                    }
                    keyCounter++;
                }
                if (reference.Attributes["URI"].Value.Contains("SignatureProperties"))
                {
                    if (reference.Attributes["URI"].Value.Equals('#' + signaturePropsAttr))
                    {
                        if (!idRef.Equals(reference.Attributes["Id"].Value) || !reference.Attributes["Type"].Value.Equals("http://www.w3.org/2000/09/xmldsig#SignatureProperties"))
                        {
                            isWrong = 13;
                            return isWrong;
                        }
                        sigCounter++;

                    }
                    else
                    {
                        isWrong = 13;
                        return isWrong;
                    }
                }
                
                if (reference.Attributes["URI"].Value.Equals('#' + signedPropsAttr))
                {
                    if (!idRef.Equals(reference.Attributes["Id"].Value) || !reference.Attributes["Type"].Value.Equals("http://uri.etsi.org/01903#SignedProperties"))
                    {
                        isWrong = 13;
                        return isWrong;
                    }
                    signedCounter++;
                }

                if (keyCounter == 2 || signedCounter == 2 || sigCounter == 2)
                {
                    isWrong = 13;
                    return isWrong;
                }

                if (reference.Attributes["URI"].Value.Contains("Manifest"))
                {
                    var manifestList = doc.SelectNodes("//ds:Manifest[@Id='" + reference.Attributes["URI"].Value.Substring(1)+"']", nsmgr);
                    if (manifestList.Count == 0 || !reference.Attributes["Type"].Value.Equals("http://www.w3.org/2000/09/xmldsig#Manifest"))
                    {
                        isWrong = 13;
                        return isWrong;
                    }
                }
            }
            return isWrong;
        }

        private int checkTimestamp14(XmlDocument doc)
        {
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
            nsmgr.AddNamespace("ds", "http://www.w3.org/2000/09/xmldsig#");
            nsmgr.AddNamespace("xzep", "http://www.ditec.sk/ep/signature_formats/xades_zep/v1.0");
            nsmgr.AddNamespace("xades", "http://uri.etsi.org/01903/v1.3.2#");
            

            var encapTS = doc.SelectNodes("//xades:EncapsulatedTimeStamp", nsmgr)[0].InnerText;

            byte[] resultData = Convert.FromBase64String(encapTS);
            //string decodedResult = Encoding.UTF8.GetString(resultData);

            var signingTime = doc.SelectNodes("//xades:SigningTime", nsmgr)[0].InnerText;

            Org.BouncyCastle.Cms.CmsSignedData decodedResponse = new Org.BouncyCastle.Cms.CmsSignedData(resultData);
            var tst = new Org.BouncyCastle.Tsp.TimeStampToken(decodedResponse);

            var sigTime = Convert.ToDateTime(signingTime);
            var genTime = tst.TimeStampInfo.GenTime;



            return 0;
        }

    }
}