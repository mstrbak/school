﻿using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Web;
using System.Xml;
using System.Xml.Linq;

namespace xsdvalid.Models
{
    public class Student
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime Birthdate { get; set; }
        public string AisId { get; set; }
        public int Attempts { get; set; }
        public string Status { get; set; }

        public bool SaveToXml()
        {
            var serverPath = HttpContext.Current.Server.MapPath("~");
            var xmlFolder = Path.Combine(serverPath, @"XMLContent");
            var xmlFile = Path.Combine(xmlFolder, @"students.xml");

            if (!File.Exists(xmlFile))
            {
                using (XmlWriter w = XmlWriter.Create(xmlFile))
                {
                    w.WriteStartElement("students");
                    w.WriteEndElement();
                }
            }
            
            XDocument doc = XDocument.Load(xmlFile);
            XElement students = doc.Element("students");
            if (students == null)
                return false;

            students.Add(
                new XElement("student", 
                    new XAttribute("ais_id", AisId), 
                    new XAttribute("status", Status),
                    new XElement("firstname", Firstname),
                    new XElement("lastname", Lastname),
                    new XElement("birthdate", Birthdate.ToString("yyyy-MM-dd")),
                    new XElement("attempts", Attempts)
                ));
            doc.Save(xmlFile);

            return true;
        }
    }

}